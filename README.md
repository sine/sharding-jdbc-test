# sharding-jdbc-test

#### 介绍

sharding-jdbc-test

shardingjdbc 4.1 分库分表，按关键字段分库，时间月份分表。 springboot2.* , yml格式配置文件。

插入和查询都是按时间分表，困难点是表无法创建。 通过解析配置文件，初始化创建表，还可以定时任务定时创建表。

#### 软件架构

软件架构说明

#### 安装教程

创建两个库， shard1, shard2 创建表语句 SQL:
CREATE TABLE `insert_month` (
`finance_no` bigint(20) NOT NULL COMMENT '流水号',
`shop_id` int(11) DEFAULT NULL COMMENT '店铺id',
`create_date_time` datetime DEFAULT NULL,
`remark` varchar(255) DEFAULT NULL, PRIMARY KEY (`finance_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

### 根据create_date_time 分表， shop_id 分库

分库 shop_id字段 对应关系 alipay:1,sales:0

分库分表逻辑在包 com.test.sharding.config.sharding

自动创建表在 com.test.sharding.scheduled.TableCreate

#### 使用说明

1. 批量插入 localhost:8092/insert
2. 插入单个 localhost:8092/one
3. 查询 http://localhost:8092/get

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
